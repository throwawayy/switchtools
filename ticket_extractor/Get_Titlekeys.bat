@echo off
title TitleKey Automator - By Conex 2018
:: see the title at the top.
echo Grabbing TitleKeys
echo :P
py -2 -m easy_install crypto
py -2 -m easy_install pycrypto
py -2 get_ticketbins.py 80000000000000e1
py -2 get_ticketbins.py 80000000000000e2
py -2 get_titlekeys.py PRODINFO.bin personal_ticketblob.bin > person_keys.txt
cls
py -2 get_titlekeys.py PRODINFO.bin common_ticketblob.bin >common_keys.txt
echo All Done!
pause