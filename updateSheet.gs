function updateSheet() {
  var url = "RAW keydb uri";
  var options = {
    "method": "GET",
    "followRedirects": true,
    "muteHttpExceptions": true
  };
  var sheet = SpreadsheetApp.getActiveSheet();
  var data = sheet.getDataRange().getValues();
  var result = UrlFetchApp.fetch(url, options);
  if (result.getResponseCode() == 200) {
    var dbraw = result.getContentText();
    var db = dbraw.split("\n");
    sheet.clearContents();
    sheet.appendRow(["UPDATING, PLEASE WAIT"]);
    var range = sheet.getRange(2, 1, db.length, 3);
    var array = [];
    for (var i = 0; i < db.length; i++) {
      array.push(["'" + db[i].split("|")[2], "'" + db[i].split("|")[0], "'" + db[i].split("|")[1]]);
    }
    range.setValues(array);
    
    sheet.getRange("A1:C1").setValues([
      ["Title", "Title ID", "Title Key"]
    ]);
    var range = sheet.getRange(1, 1, 4, sheet.getLastRow());
    range.setFontFamily("Roboto Mono");
  }
}