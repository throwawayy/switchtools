@echo off
echo.
echo Put this file to the CDSNP folder!
echo.
echo Updating TitleKeys Database...
echo.
powershell.exe -Command "py-3 CDNSP.py -updatedb"
echo Success!
echo.
echo Starting CMD with CDNSP now
echo.
echo Start with the command "CDNSP.py -h" to get help
echo.
echo.
echo.
cmd