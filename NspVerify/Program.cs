﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace NspVerify
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args[0].Equals("--nsp"))
            {
                Console.WriteLine(isNspValid(args[1], args[2]));
            }
            else if (args[0].Equals("--db"))
            {
                var file = File.ReadAllLines("keydb.nswkey");
                foreach (var line in file)
                {
                    var tid = line.Split("|")[0];
                    var tkey = line.Split("|")[1];
                    var name = line.Split("|")[2];
                    var result = isKeyValid(tid, tkey);
                    if (result)
                    {
                        Console.WriteLine($"{name} verified successfully!");
                        File.AppendAllText("_result.txt", $"{name} verified successfully!" + Environment.NewLine);
                    }
                    else
                    {
                        Console.WriteLine($"[WARN] {name} FAILED VERIFICATION ({tid} {tkey})");
                        File.AppendAllText("_result.txt", $"[WARN] {name} FAILED VERIFICATION ({tid} {tkey})" + Environment.NewLine);
                    }
                }
            }
            else
            {
                Console.WriteLine(isKeyValid(args[0], args[1]));
            }
        }


        static bool isKeyValid(string tid, string tkey, int version = 0)
        {
            run_python("CDNSP.py", $"-g {tid}-{version}-{tkey}");

            foreach (var file in Directory.GetFiles(tid))
            {
                if (!file.Contains(".nca")) continue;
                var hactooloutput = run_hactool($"{file} -k keys.txt --titlekey={tkey}");
                //Console.WriteLine(hactooloutput);
                var result = hactooloutput.Contains("Section 0:") && !hactooloutput.Contains("Error");
                if (result) continue;
                Directory.Delete(tid, true);
                return false;
            }

            Directory.Delete(tid, true);
            return true;
        }

        static bool isNspValid(string nspPath, string tkey)
        {
            var extractoutput = run_hactool($"-t pfs0 -k keys.txt --pfs0dir=nsptest {nspPath}");
            var lines = extractoutput.Split("\n");
            var file = lines[lines.Length - 3].Replace("\r", "").Replace("\t", "").Replace("\n", "").Replace("...", "")
                .Split(" to ")[1];
            var hactooloutput = run_hactool($"{file} -k keys.txt --titlekey={tkey}");
            //Console.WriteLine(hactooloutput);
            Directory.Delete("nsptest", true);
            return hactooloutput.Contains("Section 0:") && !hactooloutput.Contains("Error");
        }

        private static string run_python(string cmd, string args)
        {
            var start = new ProcessStartInfo();
            start.FileName = "py";
            start.Arguments = string.Format("-3 {0} {1}", cmd, args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (var process = Process.Start(start))
            {
                using (var reader = process.StandardOutput)
                {
                    var result = reader.ReadToEnd();
                    return result;
                }
            }
        }

        private static string run_hactool(string args)
        {
            var start = new ProcessStartInfo();
            start.FileName = "hactool";
            start.Arguments = args;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;
            using (var process = Process.Start(start))
            {
                using (var reader = process.StandardOutput)
                {
                    var result = reader.ReadToEnd();
                    using (var reader2 = process.StandardError)
                        result += reader2.ReadToEnd();
                    return result;
                }
            }
        }
    }
}