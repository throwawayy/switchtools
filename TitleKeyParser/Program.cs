﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using DiscordWebhook;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TitleKeyParser
{
    class Program
    {
        public static List<Title> db = new List<Title>();
        public const bool blockupdates = true;
        public static string changelog = "";

        static void Main(string[] args)
        {
            if (File.Exists("db.nswkey"))
            {
                var dbin = File.ReadAllLines("db.nswkey");
                foreach (var l in dbin)
                {
                    var spl = l.Split("|");
                    if (spl[2].Equals("[DLC]"))
                    {
                        spl[2] = Title.getTitle(spl[0]);
                    }

                    db.Add(new Title(spl[2], spl[0], spl[1]));
                }
            }

            var inp = File.ReadAllLines(args[1]);

            if (args[0] == "--keydump")
            {
                var lastline = 0;
                var totallines = inp.Length;
                while (lastline < totallines)
                {
                    lastline++;
                    var tid = inp[++lastline].Substring(15);
                    var tk = inp[++lastline].Substring(15);
                    lastline++;
                    if (tid.Substring(0, 16).EndsWith("800") && blockupdates)
                        continue;
                    var title = new Title(tid, tk);
                    addTitle(title);
                }
            }

            else if (args[0] == "--simple")
            {
                foreach (var line in inp)
                {
                    var split = line.Split(" ");
                    if (split[0].Substring(0, 16).EndsWith("800") && blockupdates)
                        continue;
                    var title = new Title(split[0], split[1]);

                    addTitle(title);
                }
            }
            
            var hastebinstring = "";
            foreach (var title in db)
            {
                hastebinstring += $"{title.tid}|{title.tkey}|{title.name}\r\n";
            }

            string baseUrl = "https://hastebin.com/";
            var hasteBinClient = new HasteBinClient(baseUrl);
            HasteBinResult result = hasteBinClient.Post(hastebinstring).Result;

            if (result.IsSuccess)
            {
                //Console.WriteLine($"{baseUrl}{result.Key}");
                //Console.WriteLine($"{baseUrl}raw/{result.Key}");
            }
            else
            {
                Console.WriteLine($"Posting to hastebin failed, status code was {result.StatusCode}");
            }

            try
            {
                var snip = new snipli();
                var keydburl = snip.editURL("keydb", $"{baseUrl}{result.Key}");
                var keydbrawurl = snip.editURL("keydbraw", $"{baseUrl}raw/{result.Key}");
            }
            catch
            {
                try
                {
                    Thread.Sleep(1000);
                    var snip = new snipli();
                    var keydburl = snip.editURL("keydb", $"{baseUrl}{result.Key}");
                    var keydbrawurl = snip.editURL("keydbraw", $"{baseUrl}raw/{result.Key}");
                }
                catch
                {
                    Console.WriteLine("[WARN] snip.li is down :(");
                }
            }

            
            if (changelog != "modified")
                snipli.WebhookPush("");
            Console.WriteLine("Done!");
        }

        static void addTitle(Title t)
        {
            t.tid = t.tid.ToLower();
            t.tkey = t.tkey.ToLower();
            if (db.Exists(p => p.tid.Equals(t.tid)))
            {
                if (db.Exists(p => p.tid.Equals(t.tid) && !p.tkey.Equals(t.tkey)))
                {
                    File.AppendAllText("_wtfdb.nswkey", $"{t.name};;{t.tid};;{t.tkey}\r\n");
                    Console.WriteLine($"tkey mismatch, skipping: {t.name} - {t.tid} {t.tkey}");
                    //changelog += $"tkey mismatch, skipping: {t.name} - {t.tid} {t.tkey}\n";
                }
            }
            else
            {
                if (isKeyValid(t.tid, t.tkey))
                {
                    db.Add(t);
                    Console.WriteLine($"New Entry: {t.name} - {t.tid} {t.tkey}");
                    changelog = $"New Entry: {t.name}\n";
                }
                else
                {
                    Console.WriteLine($"Invalid key, skipping: {t.name} - {t.tid} {t.tkey}");
                }
                
                db.Sort((p, q) => string.Compare(p.name, q.name, StringComparison.OrdinalIgnoreCase));

                if (File.Exists("db.nswkey"))
                    File.Delete("db.nswkey");

                if (File.Exists("db.excel.csv"))
                    File.Delete("db.excel.csv");

                if (File.Exists("db.gdoc.csv"))
                    File.Delete("db.gdoc.csv");

                File.AppendAllText("db.excel.csv", "sep=;\r\n");
                File.AppendAllText("db.gdoc.csv", "Title;Title ID;Title Key\r\n");

                foreach (var title in db)
                {
                    File.AppendAllText("db.nswkey", $"{title.tid}|{title.tkey}|{title.name}\r\n");
                    File.AppendAllText("db.excel.csv", $"{title.name};{title.tid};{title.tkey}\r\n");
                    File.AppendAllText("db.gdoc.csv", $"{title.name};{title.tid};{title.tkey}\r\n");
                }
                
                //Console.WriteLine(keydburl);
                //Console.WriteLine(keydbrawurl);
                
                if (changelog != "" && changelog != "modified")
                    snipli.WebhookPush(changelog);
                
                changelog = "modified";
            }
        }
        
        static bool isKeyValid(string tid, string tkey, int version = 0)
        {
            run_python("CDNSP.py", $"-g {tid}-{version}-{tkey}");

            bool has_run = false;
            foreach (var file in Directory.GetFiles(tid))
            {
                if (!file.Contains(".nca")) continue;
                has_run = true;
                var hactooloutput = run_hactool($"{file} -k keys.txt --titlekey={tkey}");
                //Console.WriteLine(hactooloutput);
                var result = hactooloutput.Contains("Section 0:") && !hactooloutput.Contains("Error");
                if (result) continue;
                Directory.Delete(tid, true);
                return false;
            }

            Directory.Delete(tid, true);
            return has_run;
        }
        
        private static string run_python(string cmd, string args)
        {
            var start = new ProcessStartInfo();
            start.FileName = "python3";
            start.Arguments = string.Format("{0} {1}", cmd, args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (var process = Process.Start(start))
            {
                using (var reader = process.StandardOutput)
                {
                    var result = reader.ReadToEnd();
                    return result;
                }
            }
        }

        private static string run_hactool(string args)
        {
            var start = new ProcessStartInfo();
            start.FileName = "hactool";
            start.Arguments = args;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;
            using (var process = Process.Start(start))
            {
                using (var reader = process.StandardOutput)
                {
                    var result = reader.ReadToEnd();
                    using (var reader2 = process.StandardError)
                        result += reader2.ReadToEnd();
                    return result;
                }
            }
        }
    }

    class Title
    {
        public string tid;
        public string tkey;
        public string name;

        public Title(string tid, string tkey)
        {
            this.tid = tid.ToLower().Substring(0, 16);
            this.tkey = tkey.ToLower().Substring(0, 32);
            name = getTitle(tid);
        }

        public Title(string name, string tid, string tkey)
        {
            this.tid = tid.ToLower().Substring(0, 16);
            this.tkey = tkey.ToLower().Substring(0, 32);
            this.name = name;
        }

        private static string run_cmd(string cmd, string args)
        {
            var start = new ProcessStartInfo();
            //start.FileName = "C:\\Program Files (x86)\\Python37-32\\python.exe";
            start.FileName = "python3";
            start.Arguments = string.Format("{0} {1}", cmd, args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (var process = Process.Start(start))
            {
                using (var reader = process.StandardOutput)
                {
                    var result = reader.ReadToEnd();
                    return result;
                }
            }
        }

        public static string getTitle(string tid)
        {
            tid = tid.Substring(0, 16);
            if (Program.db.Exists(p => p.tid.Equals(tid)))
            {
                return Program.db.First(p => p.tid.Equals(tid)).name;
            }

            run_cmd("CDNSP.py", $"-infodump {tid}");

            var title = "[MISSING TITLE]";
            var lines = File.ReadAllLines("infodump.txt");
            foreach (var line in lines)
            {
                if (line.Contains("Name"))
                    title = line.Split("Name: ")[1];
            }

            title = Regex.Replace(title, "[™©®]+", "");

            if (!tid.EndsWith("00"))
            {
                var tid1 = tid.Substring(0, 12);
                var tid2 = (int.Parse(tid.Substring(12, 1), NumberStyles.HexNumber) - 1).ToString("X").ToLower();
                const string tid3 = "000";


                title = "[DLC] " + getTitle(tid1 + tid2 + tid3);
            }
            else if (tid.EndsWith("800"))
                title = "[UPDATE] " + title;

            title = title.Replace("\n", "").Replace("\r", "");
            if (title.Equals(""))
                title = "[MISSING TITLE]";

            //Console.WriteLine("Processing " + title + $" [{tid}]");
            return title;
        }
    }

    public class HasteBinClient
    {
        private static HttpClient _httpClient;
        private string _baseUrl;

        static HasteBinClient()
        {
            _httpClient = new HttpClient();
        }

        public HasteBinClient(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public async Task<HasteBinResult> Post(string content)
        {
            string fullUrl = _baseUrl;
            if (!fullUrl.EndsWith("/"))
            {
                fullUrl += "/";
            }

            string postUrl = $"{fullUrl}documents";

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(postUrl));
            request.Content = new StringContent(content);
            HttpResponseMessage result = await _httpClient.SendAsync(request);

            if (result.IsSuccessStatusCode)
            {
                string json = await result.Content.ReadAsStringAsync();
                HasteBinResult hasteBinResult = JsonConvert.DeserializeObject<HasteBinResult>(json);

                if (hasteBinResult?.Key != null)
                {
                    hasteBinResult.FullUrl = $"{fullUrl}{hasteBinResult.Key}";
                    hasteBinResult.IsSuccess = true;
                    hasteBinResult.StatusCode = 200;
                    return hasteBinResult;
                }
            }

            return new HasteBinResult()
            {
                FullUrl = fullUrl,
                IsSuccess = false,
                StatusCode = (int) result.StatusCode
            };
        }
    }

// Define other methods and classes here
    public class HasteBinResult
    {
        public string Key { get; set; }
        public string FullUrl { get; set; }
        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
    }

    public class snipli
    {
        private static readonly HttpClient client = new HttpClient();
        private string accesstoken;

        public snipli()
        {
            var values = new Dictionary<string, string>
            {
                {"client_id", "<snip.li client id>"},
                {"client_secret", "<snip.li client secret>"}
            };


            var content = new FormUrlEncodedContent(values);

            var response = client.PostAsync("https://api.snipli.com/authentication", content);

            var responseString = response.Result.Content.ReadAsStringAsync().Result;
            var respJ = AuthResponse.FromJson(responseString);
            accesstoken = respJ.Data.AccessToken;
            client.DefaultRequestHeaders.Add("X-Snipli-Access-Token", accesstoken);
        }

        public string shortenURL(string longurl)
        {
            var values = new Dictionary<string, string>
            {
                {"long_url", longurl},
                {"private", "true"},
                {"private_stats", "true"}
            };


            var content = new FormUrlEncodedContent(values);
            var response = client.PostAsync("https://api.snipli.com/shorten", content);

            var responseString = response.Result.Content.ReadAsStringAsync().Result;
            var respJ = ShortenResponse.FromJson(responseString);
            return respJ.Data.Sniplink;
        }
        
        public string editURL(string shorturl, string longurl)
        {
            var values = new Dictionary<string, string>
            {
                {"short_url", shorturl},
                {"long_url", longurl},
                {"private", "true"},
                {"private_stats", "true"}
            };


            var content = new FormUrlEncodedContent(values);
            var response = client.PutAsync("https://api.snipli.com/edit/" + shorturl, content);

            var responseString = response.Result.Content.ReadAsStringAsync().Result;
            var respJ = ShortenResponse.FromJson(responseString);
            return respJ.Data.Sniplink;
        }

        public static void WebhookPush(string msg)
        {
            var webhook = new Webhook(0, //webhook id
                "<webhook secret>");
            
            //TODO: split at 2000 chars (last \n)
            if (msg.Equals(""))
                msg = "Manual update";
            
            var embed = new Embed();
            embed.Color = 16747520;
            embed.Title = "KeyDB was updated!";
            embed.Description = "" + msg + "";
            webhook.Embeds.Add(embed);

            var res = webhook.Send().Result;
        }
    }


    public partial class AuthResponse
    {
        [JsonProperty("data")] public Data Data { get; set; }

        [JsonProperty("response_code")] public long ResponseCode { get; set; }

        [JsonProperty("response_message")] public string ResponseMessage { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("access_token")] public string AccessToken { get; set; }

        [JsonProperty("expiration")] public long Expiration { get; set; }
    }

    public partial class AuthResponse
    {
        public static AuthResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<AuthResponse>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this AuthResponse self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
    
    public partial class ShortenResponse
    {
        [JsonProperty("data")]
        public Data Data { get; set; }

        [JsonProperty("response_code")]
        public long ResponseCode { get; set; }

        [JsonProperty("response_message")]
        public string ResponseMessage { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("short_url")]
        public string ShortUrl { get; set; }

        [JsonProperty("sniplink")]
        public string Sniplink { get; set; }

        [JsonProperty("long_url")]
        public string LongUrl { get; set; }
    }

    public partial class ShortenResponse
    {
        public static ShortenResponse FromJson(string json) => JsonConvert.DeserializeObject<ShortenResponse>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this ShortenResponse self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            },
        };
    }
}